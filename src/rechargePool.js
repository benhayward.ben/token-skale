require('dotenv').config()

const Web3 = require("web3");
const Tx = require("ethereumjs-tx").Transaction;

/**
 * Add funds to Community Pool.
 * https://docs.skale.network/ima/1.0.x/funding-exits
 */
(function rechargePool() {
  // !!! Amount to fund in ETH - change this for production use !!!
  const amountEth = '0.1';

  // SKALE Contract Rinkeby ABIs
  let rinkebyABIs = require("./contracts/rinkeby_ABIs.json");

  // CommunityPool contract details
  const communityPoolAddress = rinkebyABIs.community_pool_address;
  const communityPoolABI = rinkebyABIs.community_pool_abi;

  // SKALE chain name.
  let schainName = process.env.SKALE_CHAIN_NAME;
  
  // Account to charge.
  let accountForMainnet = process.env.TRANSFER_TESTING_WALLET_XPUB;

  // ID of SKALE chain as hex.
  let chainId = process.env.SKALE_CHAIN_ID_HEX;

  // private key in buffer
  let privateKey = new Buffer.from(
    process.env.TRANSFER_TESTING_WALLET_XPRIV,
    "hex"
  );

  // constructs web3 provider - Swap for mainnet RPC endpoint if needed
  let mainnet = process.env.RINKEBY_RPC_ENDPOINT;

  // web3, fixed currently to rinkeby.
  const web3 = new Web3(mainnet);

  // CommunityPool contract
  let CommunityPool = new web3.eth.Contract(
    communityPoolABI,
    communityPoolAddress
  );

  // setting up the method we are going to call
  let registerMainnetCommunityPool = CommunityPool.methods
      .rechargeUserWallet(schainName, accountForMainnet)
      .encodeABI();

  // get nonce to make tx
  web3.eth.getTransactionCount(accountForMainnet).then((nonce) => {
    //create raw transaction
    const rawRechargeUserWallet = {
      chainId: chainId,
      from: accountForMainnet,
      nonce: "0x" + nonce.toString(16),
      data: registerMainnetCommunityPool,
      to: communityPoolAddress,
      gas: 6500000,
      gasPrice: 2000000000,
      value: web3.utils.toHex(web3.utils.toWei(amountEth, "ether")) // Recharge pool with 0.5 ETH
    };

  // make and sign transaction
  const txRechargeUserWallet = new Tx(rawRechargeUserWallet, {
        chain: "rinkeby",
        hardfork: "petersburg"
      });
      txRechargeUserWallet.sign(privateKey);

  //serialize transaction
  const serializedRechargeUserWallet = txRechargeUserWallet.serialize();

  //send signed transaction
  web3.eth
    .sendSignedTransaction(
      "0x" + serializedRechargeUserWallet.toString("hex")
    )
    .on("receipt", (receipt) => {
      //record receipt to console
      console.log(receipt);
    })
    .catch(console.error);
  });
})();
