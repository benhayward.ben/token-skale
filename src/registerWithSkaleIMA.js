require('dotenv').config()

const Common = require('ethereumjs-common').default; 
const Web3 = require("web3");
const Tx = require("ethereumjs-tx").Transaction;

(function registerOnSchain() {
  let schainABIs = require("./contracts/schain_ABIs.json");
  
  let privateKey = new Buffer.from(
    process.env.SKALE_XPRIV,
    "hex"
  );
  let erc20OwnerForSchain = process.env.SKALE_XPUB;

  let schain = process.env.SKALE_RPC_ENDPOINT;
  let chainId = process.env.SKALE_CHAIN_ID_HEX;

  const customCommon = Common.forCustomChain(
    "mainnet",
    {
      name: "skale-network",
      chainId: chainId
    },
    "istanbul"
  );

  const tokenManagerAddress = schainABIs.token_manager_erc20_address;
  const tokenManagerABI = schainABIs.token_manager_erc20_abi;

  const erc20AddressOnMainnet = process.env.RINKEBY_ERC20_CONTRACT;
  const erc20AddressOnSchain = process.env.SKALE_ERC20_CONTRACT;

  const web3ForSchain = new Web3(schain);

  let TokenManager = new web3ForSchain.eth.Contract(
    tokenManagerABI,
    tokenManagerAddress
  );

  /**
   * Uses the SKALE TokenManagerERC20
   * contract function addERC20TokenByOwner
   */
  let addERC20TokenByOwner = TokenManager.methods
    .addERC20TokenByOwner("Mainnet", erc20AddressOnMainnet, erc20AddressOnSchain)
    .encodeABI();

  web3ForSchain.eth.getTransactionCount(erc20OwnerForSchain).then((nonce) => {
    const rawTxAddERC20TokenByOwner = {
      from: erc20OwnerForSchain,
      nonce: "0x" + nonce.toString(16),
      data: addERC20TokenByOwner,
      to: tokenManagerAddress,
      gas: 6500000,
      gasPrice: 100000000000,
      value: 0
    };

    //sign transaction
    const txAddERC20TokenByOwner = new Tx(rawTxAddERC20TokenByOwner, {
      common: customCommon
    });

    txAddERC20TokenByOwner.sign(privateKey);

    const serializedTxDeposit = txAddERC20TokenByOwner.serialize();

    //send signed transaction (addERC20TokenByOwner)
    web3ForSchain.eth
      .sendSignedTransaction("0x" + serializedTxDeposit.toString("hex"))
      .on("receipt", (receipt) => {
        console.log(receipt);
      })
      .catch(console.error);
  })})();
