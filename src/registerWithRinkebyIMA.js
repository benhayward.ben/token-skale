require('dotenv').config()

const Web3 = require("web3");
const Tx = require("ethereumjs-tx").Transaction;

(function registerOnMainnet() {
  let rinkebyABIs = require("./contracts/rinkeby_ABIs.json");

  // USES SKALE WALLET.
  let privateKey = new Buffer.from(
    process.env.SKALE_XPRIV,
    "hex"
  );

  let erc20OwnerForMainnet =
    process.env.SKALE_XPUB;

  let chainId = process.env.RINKEBY_CHAIN_ID;
 
  let rinkeby = process.env.RINKEBY_RPC_ENDPOINT;
  let schainName = process.env.SKALE_CHAIN_NAME;

  const depositBoxAddress = rinkebyABIs.deposit_box_erc20_address;
  const depositBoxABI = rinkebyABIs.deposit_box_erc20_abi;

  // RINKEBY
  const erc20AddressOnMainnet = process.env.RINKEBY_ERC20_CONTRACT;

  const web3ForMainnet = new Web3(rinkeby);

  let DepositBox = new web3ForMainnet.eth.Contract(
    depositBoxABI,
    depositBoxAddress
  );

  /**
   * Uses the SKALE DepositBoxERC20
   * contract function addERC20TokenByOwner
   */
  let addERC20TokenByOwner = DepositBox.methods
      .addERC20TokenByOwner(schainName, erc20AddressOnMainnet)
      .encodeABI();

  web3ForMainnet.eth.net.getId(console.log)
  web3ForMainnet.eth.getTransactionCount(erc20OwnerForMainnet).then((nonce) => {
    const rawTxAddERC20TokenByOwner = {
      chainId: chainId,
      from: erc20OwnerForMainnet,
      nonce: "0x" + nonce.toString(16),
      data: addERC20TokenByOwner,
      to: depositBoxAddress,
      gas: 6500000,
      gasPrice: 100000000000,
      value: 0
    };

    //sign transaction
    const txAddERC20TokenByOwner = new Tx(rawTxAddERC20TokenByOwner, {
      chain: "rinkeby",
      hardfork: "petersburg"
    });

    txAddERC20TokenByOwner.sign(privateKey);

    const serializedTxDeposit = txAddERC20TokenByOwner.serialize();

    //send signed transaction (addERC20TokenByOwner)
    web3ForMainnet.eth
      .sendSignedTransaction("0x" + serializedTxDeposit.toString("hex"))
      .on("receipt", (receipt) => {
        console.log(receipt);
      })
      .catch((e) => {
        console.error(e);
      });
  });
})();
