pragma solidity ^0.5.17;

import "@openzeppelin/contracts/token/ERC20/ERC20Burnable.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20Detailed.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20Mintable.sol";

contract MindsToken is ERC20Burnable, ERC20Detailed, ERC20Mintable {

    constructor(
      string memory _name,
      string memory _symbol,
      uint256 _decimals
    )
    ERC20Detailed(_name, _symbol, 18)
    public
    {
    }
    
    function approveAndCall(address _spender, uint256 _value, bytes memory _extraData) public {
         _approve(msg.sender, _spender, _value);

        (bool success,) = _spender.call(
            abi.encodeWithSignature(
                "receiveApproval(address,uint256,address,bytes)",
                msg.sender,
                _value,
                address(this),
                _extraData
            )
        );

        require(success, "Call failed");
    }
}
