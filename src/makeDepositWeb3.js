require('dotenv').config()

const Web3 = require("web3");
const Tx = require("ethereumjs-tx").Transaction;

(function makeDeposit() {

  // amount to deposit.
  const DEPOSIT_AMOUNT_ETH = '1';

  // IMA ABI - Rinkeby or mainnet
  let imaABIs = require("./contracts/rinkeby_ABIs.json");
  
  // ABI for our Rinkeby Token.
  let rinkebyERC20ABI = require("./contracts/rinkeby_ERC20_ABI.json");

  // private key in buffer for wallet we want to transfer from.
  let privateKey = new Buffer.from(
    process.env.TRANSFER_TESTING_WALLET_XPRIV,
    "hex"
  );

  // public key for wallet we want to transfer from.
  let accountForMainnet = process.env.TRANSFER_TESTING_WALLET_XPUB;

  // constructs web3 provider - Swap for mainnet RPC endpoint if needed
  let rinkeby = process.env.RINKEBY_RPC_ENDPOINT;

  // SKALE chain name and chain ID as hex
  let schainName = process.env.SKALE_CHAIN_NAME;
  let chainId = process.env.SKALE_CHAIN_ID_HEX;

  // SKALE deposit box information from ABI
  const depositBoxAddress = imaABIs.deposit_box_erc20_address;
  const depositBoxABI = imaABIs.deposit_box_erc20_abi;

  // ABI parsed from JSON and contract address on Rinkeby.
  const erc20ABI = rinkebyERC20ABI.abi;
  const erc20Address = process.env.RINKEBY_ERC20_CONTRACT;

  // construct web3 with provider.
  const web3ForMainnet = new Web3(rinkeby);

  // instantiate SKALE deposit box as object.
  let depositBox = new web3ForMainnet.eth.Contract(
    depositBoxABI,
    depositBoxAddress
  );

  // construct our ERC20 version of Minds contract.
  let contractERC20 = new web3ForMainnet.eth.Contract(erc20ABI, erc20Address);

  /**
   * Uses the openzeppelin ERC20
   * contract function approve
   * https://github.com/OpenZeppelin/openzeppelin-contracts/tree/master/contracts/token/ERC20
   */
  let approve = contractERC20.methods
    .approve(
      depositBoxAddress,
      web3ForMainnet.utils.toHex(web3ForMainnet.utils.toWei(DEPOSIT_AMOUNT_ETH, "ether"))
    )
    .encodeABI();

  /**
   * Uses the SKALE DepositBox
   * contract function depositERC20
   */
  let deposit = depositBox.methods
    .depositERC20(
      schainName,
      erc20Address,
      web3ForMainnet.utils.toHex(web3ForMainnet.utils.toWei(DEPOSIT_AMOUNT_ETH, "ether")) // amount
    )
    .encodeABI();

  web3ForMainnet.eth.getTransactionCount(accountForMainnet).then((nonce) => {
    //create raw transaction
    const rawTxApprove = {
      chainId: chainId,
      from: accountForMainnet,
      nonce: "0x" + nonce.toString(16),
      data: approve,
      to: erc20Address,
      gas: 6500000,
      gasPrice: 2000000000
    };

    //sign transaction
    const txApprove = new Tx(rawTxApprove, {
      chain: "rinkeby",
      hardfork: "petersburg"
    });
    txApprove.sign(privateKey);

    const serializedTxApprove = txApprove.serialize();

    //send signed transaction (approve)
    web3ForMainnet.eth
      .sendSignedTransaction("0x" + serializedTxApprove.toString("hex"))
      .on("receipt", (receipt) => {
        console.log(receipt);
        web3ForMainnet.eth
          .getTransactionCount(accountForMainnet)
          .then((nonce) => {
            const rawTxDeposit = {
              chainId: chainId,
              from: accountForMainnet,
              nonce: "0x" + nonce.toString(16),
              data: deposit,
              to: depositBoxAddress,
              gas: 6500000,
              gasPrice: 2000000000
            };

            //sign transaction
            const txDeposit = new Tx(rawTxDeposit, {
              chain: "rinkeby",
              hardfork: "petersburg"
            });

            txDeposit.sign(privateKey);

            const serializedTxDeposit = txDeposit.serialize();

            //send signed transaction (deposit)
            web3ForMainnet.eth
              .sendSignedTransaction("0x" + serializedTxDeposit.toString("hex"))
              .on("receipt", (receipt) => {
                console.log(receipt);
              })
              .catch(console.error);
          });
      })
      .catch(console.error);
  });
})();
