require('dotenv').config()

const Common = require('ethereumjs-common').default; 
const Tx = require("ethereumjs-tx").Transaction;
const Web3 = require("web3");

(function addMinter() {
  // SKALE IMA ABI json file.
  let schainABIs = require("./contracts/schain_ABIs.json");
  
  // SKALE ERC20 ABI json file.
  let schainERC20ABI = require("./contracts/schain_ERC20_ABI.json");
  
  // Private key in hex buffer.
  let contractOwnerPrivateKey = new Buffer.from(
    process.env.SKALE_XPRIV,
    "hex"
  );

  // Public address of owner.
  let contractOwnerAccount =
    process.env.SKALE_XPUB;

  // SKALE RPC endpoint
  let schainEndpoint = process.env.SKALE_RPC_ENDPOINT;
  
  // SKALE chain id in hex form.
  let chainId = process.env.SKALE_CHAIN_ID_HEX;

  // Common object for custom chain.
  const customCommon = Common.forCustomChain(
    "mainnet",
    {
      name: "skale-network",
      chainId: chainId
    },
    "istanbul"
  );

  // ERC20 ABI for SKALE version of contract.
  const erc20ABI = schainERC20ABI.abi;
  
  // ERC20 contract address for SKALE version of contract.
  const erc20Address = process.env.SKALE_ERC20_CONTRACT;

  // SKALE token managers ERC20 address.
  const tokenManagerERC20Address = schainABIs.token_manager_erc20_address;

  // construct web3 with SKALE chain
  const web3ForSchain = new Web3(schainEndpoint);

  // load in SKALE version of contract as object.
  let schainERC20Contract = new web3ForSchain.eth.Contract(
    erc20ABI,
    erc20Address
  );

  /**
   * Uses the openzeppelin ERC20Mintable
   * contract function addMinter
   * https://github.com/OpenZeppelin/openzeppelin-contracts/tree/master/contracts/token/ERC20
   */
  let addMinter = schainERC20Contract.methods
    .addMinter(tokenManagerERC20Address)
    .encodeABI();

  // get nonce
  web3ForSchain.eth.getTransactionCount(contractOwnerAccount).then((nonce) => {
    //create raw transaction
    const rawTxAddMinter = {
      chainId: chainId,
      from: contractOwnerAccount,
      nonce: nonce,
      data: addMinter,
      to: erc20Address,
      gasPrice: 100000000000,
      gas: 8000000,
      value: 0
    };

    //sign transaction, use custom chain Common object as param. 
    const txAddMinter = new Tx(rawTxAddMinter, { common: customCommon });
    txAddMinter.sign(contractOwnerPrivateKey);

    const serializedTxAddMinter = txAddMinter.serialize();

    //send signed transaction (add minter)
    web3ForSchain.eth
      .sendSignedTransaction("0x" + serializedTxAddMinter.toString("hex"))
      .on("receipt", (receipt) => {
        console.log(receipt);
      })
      .catch(console.error);
  })})();
