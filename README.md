# Setup

Create a .env file in the root directory containing.

```
RINKEBY_RPC_ENDPOINT=""
RINKEBY_MNEMONIC=""
RINKEBY_CHAIN_ID=4
RINKEBY_ERC20_CONTRACT="0xf5f7ad7d2c37cae59207af43d0beb4b361fb9ec8"

SKALE_XPRIV=""
SKALE_XPUB=""
SKALE_RPC_ENDPOINT=""
SKALE_CHAIN_ID=""
SKALE_CHAIN_ID_HEX=""
SKALE_CHAIN_NAME=""
SKALE_ERC20_CONTRACT="0x4E8792878Bd0CC7a19d2a5Bb345Ab18a6624c866"

# Wallet for testing exits and deposits to and from skale chain, 
# and recharge community pool
# Be sure to recharge the community pool PRIOR to testing.
TRANSFER_TESTING_WALLET_XPUB=""
TRANSFER_TESTING_WALLET_XPRIV=""
```
