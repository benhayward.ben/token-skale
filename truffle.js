require('dotenv').config()

const HDWalletProvider = require("@truffle/hdwallet-provider");

module.exports = {
  contracts_directory: "./src/contracts",
  networks: {
    // uses env mnemonic
    rinkeby_remote: {
      provider: function() { 
       return new HDWalletProvider(process.env.RINKEBY_MNEMONIC, process.env.RINKEBY_RPC_ENDPOINT);
      },
      network_id: 4,
      gas: 4500000,
      gasPrice: 10000000000,
    },
    skale: {
      provider: () => new HDWalletProvider(process.env.SKALE_XPRIV, process.env.SKALE_RPC_ENDPOINT),
      gasPrice: 0,
      network_id: process.env.SKALE_CHAIN_ID
    }
  },
  compilers: {
    solc: {
      // prevent truffle / solidity version mismatch.
      version: "^0.5.17",
    }
  }
};
